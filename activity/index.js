console.log("Hello World")

/*
	1. Create variables to store to the following user details:

	-first name - String
	-last name - String
	-age - Number
	-hobbies - Array
	-work address - Object

		-The hobbies array should contain at least 3 hobbies as Strings.
		-The work address object should contain the following key-value pairs:

			houseNumber: <value>
			street: <value>
			city: <value>
			state: <value>

	Log the values of each variable to follow/mimic the output.

	Note:
		-Name your own variables but follow the conventions and best practice in naming variables.
		-You may add your own values but keep the variable names and values Safe For Work.
*/

	//Add your variables and console log for objective 1 here:




/*			
	2. Debugging Practice - Identify and implement the best practices of creating and using variables 
	   by avoiding errors and debugging the following codes:

			-Log the values of each variable to follow/mimic the output.
*/	
    let firstName = 'Ernest Russel';
    let lastName = "Viray";
    console.log(firstName)
    console.log(lastName)

	let fullName = firstName +' ' + lastName;
	
	let age = 32;
	console.log(age);

    const hobbies = ['Jogging', 'Eating', 'Hiking', 'Motorcycling'];
    console.log("Hobbies:");
    console.log(hobbies);

    const workAddress = {
    houseNumber: 15,
    street: "Cristi",
    city: "San Mateo",
    state: "Rizal"

    }

    console.log("Work Address:");
    console.log(workAddress);


    console.log("My full name is: " + firstName + lastName);
    console.log("My current age is: " + age);


	
	const friends = ["Regie","Alexis","Mary","Nathania","Ronalyn","Mariel"];
	console.log("My Friends are: " + friends);
    friends[1] = 'Ricky'

	const profile = {

		username: "ItsyBitsy",
        fullName: firstName + " " + lastName,
        age: 32,
		isActive: false,

	}
	console.log("My Full Profile: ")
    console.log(profile)
	
	let bestFriend = "Ricky Cabral";
	console.log("My bestfriend is: " + bestFriend);

	const lastLocation = "Arctic Ocean";
	
	console.log("I was found frozen in: " + lastLocation);
